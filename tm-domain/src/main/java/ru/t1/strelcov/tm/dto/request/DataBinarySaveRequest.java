package ru.t1.strelcov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBinarySaveRequest extends AbstractUserRequest {

    public DataBinarySaveRequest(@Nullable String token) {
        super(token);
    }

}
