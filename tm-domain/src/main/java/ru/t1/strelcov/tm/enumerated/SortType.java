package ru.t1.strelcov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.comparator.ComparatorByCreated;
import ru.t1.strelcov.tm.comparator.ComparatorByDateStart;
import ru.t1.strelcov.tm.comparator.ComparatorByName;
import ru.t1.strelcov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

@Getter
public enum SortType {

    NAME("Sort by name", "name", ComparatorByName.getInstance()),
    STATUS("Sort by status", "status", ComparatorByStatus.getInstance()),
    CREATED("Sort by created", "created", ComparatorByCreated.getInstance()),
    START_DATE("Sort by start date", "start_date", ComparatorByDateStart.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final String dataBaseName;

    @NotNull
    private final Comparator comparator;

    public static boolean isValidByName(@Nullable final String name) {
        for (final SortType sortType : values()) {
            if (sortType.name().equals(name))
                return true;
        }
        return false;
    }

    SortType(@NotNull final String displayName, @NotNull String dataBaseName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.dataBaseName = dataBaseName;
        this.comparator = comparator;
    }

}
