package ru.t1.strelcov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBase64SaveRequest extends AbstractUserRequest {

    public DataBase64SaveRequest(@Nullable final String token) {
        super(token);
    }

}
