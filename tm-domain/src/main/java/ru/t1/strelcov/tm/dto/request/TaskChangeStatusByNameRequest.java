package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class TaskChangeStatusByNameRequest extends AbstractUserRequest {

    @Nullable
    private String status;

    @Nullable
    private String name;

    public TaskChangeStatusByNameRequest(@Nullable final String token, @Nullable final String name, @Nullable final String status) {
        super(token);
        this.status = status;
        this.name = name;
    }

}
