package ru.t1.strelcov.tm.service.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.dto.IAuthDTOService;
import ru.t1.strelcov.tm.api.service.dto.IUserDTOService;
import ru.t1.strelcov.tm.dto.model.SessionDTO;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.AbstractException;
import ru.t1.strelcov.tm.service.ConnectionService;
import ru.t1.strelcov.tm.service.PropertyService;
import ru.t1.strelcov.tm.util.CryptUtil;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class AuthDTOServiceTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(connectionService, propertyService);

    @NotNull
    private static final IAuthDTOService authService = new AuthDTOService(userService, propertyService);

    @NotNull
    private static final String LOGIN_UNIQUE = "Login_Unique";

    @NotNull
    private static final String PASS = "PASS";

    @NotNull
    private static final String PASS_HASH = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), PASS);

    @NotNull
    private final static List<UserDTO> users = Arrays.asList(
            new UserDTO("test1", PASS_HASH, Role.USER),
            new UserDTO("test2", PASS_HASH, Role.ADMIN));

    @Before
    public void before() {
        for (@NotNull final UserDTO user : users) {
            try {
                @NotNull UserDTO existedUser = userService.findByLogin(user.getLogin());
                userService.removeByLogin(existedUser.getLogin());
                userService.removeByLogin(LOGIN_UNIQUE);
            } catch (@NotNull Exception ignored) {
            }
        }
        try {
            userService.removeByLogin(LOGIN_UNIQUE);
        } catch (@NotNull Exception ignored) {
        }
        userService.addAll(users);
    }

    @After
    public void after() {
        try {
            for (@NotNull final UserDTO user : users)
                userService.removeById(user.getId());
        } catch (@NotNull Exception ignored) {
        }
        try {
            userService.removeByLogin(LOGIN_UNIQUE);
        } catch (@NotNull Exception ignored) {
        }
    }

    @AfterClass
    public static void afterClass() {
        connectionService.closeFactory();
    }

    @SneakyThrows
    @Test
    public void loginTest() {
        Assert.assertThrows(AbstractException.class, () -> authService.login(null, "notExistedId"));
        Assert.assertThrows(AbstractException.class, () -> authService.login("notExistedId", null));
        Assert.assertThrows(AbstractException.class, () -> authService.login(users.get(0).getLogin(), "INCORRECT_PASS"));
        @NotNull final UserDTO user = users.get(0);
        @NotNull final String token = authService.login(user.getLogin(), PASS);
        @NotNull final String sessionKey = propertyService.getSessionSecret();
        @NotNull final String json = CryptUtil.decrypt(token, sessionKey);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        Assert.assertEquals(user.getId(), session.getUserId());
        Assert.assertEquals(user.getRole(), session.getRole());
        Assert.assertNotNull(session.getDate());
        userService.lockUserByLogin(users.get(0).getLogin());
        Assert.assertThrows(AbstractException.class, () -> authService.login(users.get(0).getLogin(), PASS));
    }

    @Test
    public void logoutTest() {
    }

    @SneakyThrows
    @Test
    public void validateTokenTest() {
        Assert.assertThrows(AbstractException.class, () -> authService.validateToken("notExistedId"));
        @NotNull final UserDTO user = users.get(0);
        @NotNull final String token = authService.login(user.getLogin(), PASS);
        @NotNull final SessionDTO session = authService.validateToken(token);
        Assert.assertEquals(user.getId(), session.getUserId());
        Assert.assertEquals(user.getRole(), session.getRole());
        Assert.assertNotNull(session.getDate());
        @NotNull final String sessionKey = propertyService.getSessionSecret();
        @NotNull String json = CryptUtil.decrypt(token, sessionKey);
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO sessionFromToken = objectMapper.readValue(json, SessionDTO.class);
        Assert.assertEquals(sessionFromToken, session);
        Assert.assertNotNull(sessionFromToken.getDate());
        sessionFromToken.setDate(new Date(sessionFromToken.getDate().getTime() - propertyService.getSessionTimeout() * 1000 * 60 - 10000 * 60));
        objectMapper = new ObjectMapper();
        json = objectMapper.writeValueAsString(sessionFromToken);
        @NotNull final String changedDateToken = CryptUtil.encrypt(json, sessionKey);
        Assert.assertThrows(AbstractException.class, () -> authService.validateToken(changedDateToken));
    }

}
