package ru.t1.strelcov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.model.IUserRepository;
import ru.t1.strelcov.tm.api.service.IConnectionService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.model.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.entity.UserAdminLockException;
import ru.t1.strelcov.tm.exception.entity.UserNotFoundException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.repository.model.UserRepository;
import ru.t1.strelcov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    protected final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    public IUserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash, email);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final User user;
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user = new User(login, passwordHash, role);
        add(user);
        return user;
    }

    @SneakyThrows
    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            return Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(EntityNotFoundException::new);
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.removeByLogin(login)).orElseThrow(UserNotFoundException::new);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public User updateById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @NotNull
    @Override
    public User updateByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            userRepository.update(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void changePasswordById(@Nullable final String id, @Nullable final String password) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
            user.setPasswordHash(passwordHash);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            if (user.getRole() == Role.ADMIN) throw new UserAdminLockException();
            user.setLock(true);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IUserRepository userRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            user.setLock(false);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
