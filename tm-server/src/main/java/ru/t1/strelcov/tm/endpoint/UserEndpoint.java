package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.api.service.dto.IUserDTOService;
import ru.t1.strelcov.tm.dto.model.SessionDTO;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.UserAdminRemoveException;

import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IUserEndpoint")
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserDTOService getUserService() {
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return serviceLocator.getUserService();
    }

    @Override
    @NotNull
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        @NotNull final SessionDTO session = check(request);
        getUserService().changePasswordById(session.getUserId(), request.getPassword());
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    public UserListResponse listUser(@NotNull final UserListRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        @NotNull List<UserDTO> list = getUserService().findAll();
        return new UserListResponse(list);
    }

    @NotNull
    @Override
    public UserRegisterResponse registerUser(@NotNull final UserRegisterRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        @NotNull UserDTO user = getUserService().add(request.getLogin(), request.getPassword(), Role.valueOf(request.getRole()));
        return new UserRegisterResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveByLoginResponse removeUserByLogin(@NotNull final UserRemoveByLoginRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        if ("admin".equals(request.getLogin())) throw new UserAdminRemoveException();
        @NotNull UserDTO user = getUserService().removeByLogin(request.getLogin());
        return new UserRemoveByLoginResponse(user);
    }

    @NotNull
    @Override
    public UserUpdateByLoginResponse updateUserByLogin(@NotNull final UserUpdateByLoginRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        @NotNull UserDTO user = getUserService().updateByLogin(request.getLogin(), request.getFirstName(), request.getLastName(), null, request.getEmail());
        return new UserUpdateByLoginResponse(user);
    }


    @NotNull
    @Override
    public UserUnlockByLoginResponse unlockByLogin(@NotNull final UserUnlockByLoginRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        getUserService().unlockUserByLogin(request.getLogin());
        return new UserUnlockByLoginResponse();
    }

    @NotNull
    @Override
    public UserLockByLoginResponse lockByLogin(@NotNull final UserLockByLoginRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        getUserService().lockUserByLogin(request.getLogin());
        return new UserLockByLoginResponse();
    }

}
