package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.service.dto.IAuthDTOService;
import ru.t1.strelcov.tm.api.service.dto.IProjectDTOService;
import ru.t1.strelcov.tm.api.service.dto.ITaskDTOService;
import ru.t1.strelcov.tm.api.service.dto.IUserDTOService;

public interface ServiceLocator {

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IAuthDTOService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDataService getDataService();

    @NotNull
    IConnectionService getConnectionService();

}
