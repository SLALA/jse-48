package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class CorruptCommandException extends AbstractException {

    public CorruptCommandException() {
        super("Error: Cannot registry command without name.");
    }

}
