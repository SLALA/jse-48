package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.UserListRequest;
import ru.t1.strelcov.tm.enumerated.Role;

import java.util.List;

public final class UserListCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "List users.";
    }

    @Override
    public void execute() {
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        System.out.println("[LIST USERS]");
        @NotNull final List<UserDTO> users = userEndpoint.listUser(new UserListRequest(getToken())).getList();
        int index = 1;
        for (final UserDTO user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
