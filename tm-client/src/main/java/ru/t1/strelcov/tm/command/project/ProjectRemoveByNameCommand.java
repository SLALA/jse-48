package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.request.ProjectRemoveByNameRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        projectEndpoint.removeByNameProject(new ProjectRemoveByNameRequest(getToken(), name));
    }

}
