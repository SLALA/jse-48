package ru.t1.strelcov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String arg);

    void add(@NotNull AbstractCommand command);

}
